# AGGREGATE MICRO SERVICE CONSOLE

## Console

```sh
$ bin/app ams

Psr-4 (no trailing slash !!!): ECommerce
Service vendor folder path (relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-user-address
Service name (CamelCase !!!): UserAddress
Service prefix (lower case !!!): ua
Related services vendor folders paths (coma separated, relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-user,vendor/paneric/e-commerce-address
Related services names (CamelCase, coma separated !!!): User,Address
Related services prefixes (lower case, coma separated !!!): usr,adr
Other services names (CamelCase, coma separated !!!): ListCountry,ListTypeCompany,Credential
DB table name: user_addresss

                                                                          
  AGGREGATE MICRO SERVICE UPDATE SUCCESS:                                  
                                                                          

  Resources update with vendor "ECommerce", service "UserAddress" and prefix "ua" success. 

```

*src/UserAddressApi/config/dependencies/repository.php*
*src/UserAddressApp/config/dependencies/repository.php*
```php
return [
    'user_address_repository' => static function (Container $container): UserAddressRepository
    {
        return new UserAddressRepository(
            $container->get(Manager::class),
            [
                'table' => 'user_addresss',//{aggregate_table}
                'dao_class' => UserAddressDAO::class,
                'create_unique_where' => sprintf(
                    ' %s',
                    'WHERE ua_address_id=:ua_address_id'
                ),
                'update_unique_where' => sprintf(
                    ' %s',
                    'WHERE ua_address_id=:ua_address_id'
                ),
            ]
        );
    },

    'user_address_query' => static function (Container $container): UserAddressQuery
    {
        return new UserAddressQuery(
            $container->get(Manager::class),
            [
                'adao_class' => UserAddressADAO::class,
                'base_query' => '
                    SELECT * FROM user_addresss bt
                        JOIN users st1 ON bt.ua_user_id = st1.usr_id
                            JOIN credentials st1_1 ON st1.usr_credential_id = st1_1.crd_id
                        JOIN addresss st2 ON bt.ua_address_id = st2.adr_id
                            JOIN list_countrys st2_1 ON st2.adr_list_country_id = st2_1.lc_id
                            JOIN list_type_companys st2_2 ON st2.adr_list_type_company_id = st2_2.ltc_id                
                ',
            ]
        );
    },
];
```
