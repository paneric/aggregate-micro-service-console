<?php

return [

    'related_prefixes' => [
        'template' =>
<<<END
,'{sub_prefix}'
END,
        'patterns' => ['/{sub_prefix}/'],
        'methods'  => [           'set'],
        'values'   => [     'subPrefix'],
    ],
];
