<?php

return [

    'use' => [
    //--------
        'template' =>
<<<END
use {Paneric}\{SubService}\Gateway\{SubService}DAO; \n
END,
        'patterns' => ['/{Paneric}/', '/{SubService}/'],
        'methods'  => [        'set',            'set'],
        'values'   => [     'Vendor',     'SubService'],
    ],


    'construct' => [
    //--------------
        'template' =>
<<<END
        \$this->{subService} = new {SubService}DAO();
        \$this->values = \$this->{subService}->hydrate(\$this->values); \n
END,
        'patterns' => ['/{SubService}/', '/{subService}/'],
        'methods'  => [           'set',        'set_lcf'],
        'values'   => [    'SubService',     'SubService'],
    ],


    'attributes' => [
    //---------------
        'template' =>
<<<END
    protected \${subService}; \n
END,
        'patterns' => ['/{subService}/'],
        'methods'  => [       'set_lcf'],
        'values'   => [    'SubService'],
    ],


    'getters' => [
    //------------
        'template' =>
<<<END
    public function get{SubService}(): {SubService}DAO
    {
        return \$this->{subService};
    } \n
END,
        'patterns' => ['/{SubService}/', '/{subService}/'],
        'methods'  => [           'set',        'set_lcf'],
        'values'   => [    'SubService',     'SubService'],
    ],


    'setters' => [
    //------------
        'template' =>
<<<END
    public function set{SubService}({SubService} \${subService}): void
    {
        \$this->{subService} = \${subService};
    } \n
END,
        'patterns' => ['/{SubService}/', '/{subService}/'],
        'methods'  => [           'set',        'set_lcf'],
        'values'   => [    'SubService',     'SubService'],
    ],


    'transform' => [
    //--------------
        'template' =>
<<<END
            '{sub_service}' => \$this->{subService}->convert(\$isSc), \n
END,
        'patterns' => ['/{sub_service}/', '/{subService}/'],
        'methods'  => [      'set_lc_sc',        'set_lcf'],
        'values'   => [     'SubService',     'SubService'],
    ],
];
