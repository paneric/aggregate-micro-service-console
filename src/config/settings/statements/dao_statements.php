<?php

return [

    'attributes_id' => [
    //------------------
        'template' =>
<<<END
    protected \${subService}Id; \n
END,
        'patterns' => ['/{subService}/'],
        'methods'  => [       'set_lcf'],
        'values'   => [    'SubService'],
    ],


    'getters_id' => [
    //---------------
        'template' =>
<<<END
    /**
     * @return null|int|string
     */
    public function get{SubService}Id()
    {
        return \$this->{subService}Id;
    } \n
END,
        'patterns' => ['/{SubService}/', '/{subService}/'],
        'methods'  => [           'set',        'set_lcf'],
        'values'   => [    'SubService',     'SubService'],
    ],


    'setters_id' => [
    //---------------
        'template' =>
<<<END
    /**
     * @var int|string
     */
    public function set{SubService}Id(\${subService}Id): void
    {
        \$this->{subService}Id = \${subService}Id;
    } \n
END,
        'patterns' => ['/{SubService}/', '/{subService}/'],
        'methods'  => [           'set',        'set_lcf'],
        'values'   => [    'SubService',     'SubService'],
    ],

];
