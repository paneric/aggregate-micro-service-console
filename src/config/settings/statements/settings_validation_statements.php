<?php

return [
    'attributes_id' => [
        'template' =>
<<<END
                    '{sub_service}_id' => [
                    ], \n
END,
        'patterns' => ['/{sub_service}/'],
        'methods'  => [      'set_lc_sc'],
        'values'   => [     'SubService'],
    ],


    'attributes' => [
        'template' =>
<<<END
                    '{attribute}' => [
                    ], \n
END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

];
