<?php

return [
    'vendor_related_services_paths' => [
        'template' =>
<<<END
    '{vendorRelatedServicePath}', \n
END,
        'patterns' => ['/{vendorRelatedServicePath}/'],
        'methods'  => [                         'set'],
        'values'   => [    'vendorRelatedServicePath'],
    ],
];
