<?php

return [

    'attributes_id' => [
        'template' =>
<<<END
            <div class="form-group select {{ vld_style('{sub_service}_id') }}">
                <select class="form-group-item form-group-select" name="{sub_service}_id" autocomplete="off" required>
                    {% set {subprefix}_init = {subprefix}s[19] %}
                    <option value="{{ {subprefix}_init.id }}">{{ attribute({subprefix}_init, local)|trans }}</option>
                    {% for {subprefix} in {subprefix}s %}
                        {% if {prefix}.{sub_service}_id == {subprefix}.id %}
                            <option value="{{ {subprefix}.id }}" selected>{{ attribute({subprefix}, local) }}</option>
                        {% else %}
                            <option value="{{ {subprefix}.id }}">{{ attribute({subprefix}, local) }}</option>
                        {% endif %}
                    {% endfor %}
                </select>
                <label for="name" class="form-group-item form-group-label-select"><span>{{ ('{sub_service}')|trans }}</span></label>
            </div>
            {{ vld_feedback('{sub_service}_id')|raw }}


END,
        'patterns' => ['/{sub_service}/', '/{subprefix}/'],
        'methods'  => [      'set_lc_sc',           'set'],
        'values'   => [     'SubService',     'subprefix'],
    ],


    'attributes' => [
        'template' =>
<<<END
            <div class="form-group  {{ vld_style('{attribute}') }}">
                <input class="form-group-item form-group-input" type="text" name="{attribute}" value="{{ {prefix}.{attribute} }}" autocomplete="off" required/>
                <label for="name" class="form-group-item form-group-label"><span>{{ '{attribute}'|trans }}</span></label>
            </div>
            {{ vld_feedback('{attribute}')|raw }}


END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

];
