<?php

return [

    'title_attribute' => [
        'template' =>
<<<END
                    <th class="text-center">{{ '{attribute}'|trans }}</th>

END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

    'title_attribute_id' => [
        'template' =>
<<<END
                    <th class="text-center">{{ '{sub_service}'|trans }}</th>

END,
        'patterns' => ['/{sub_service}/'],
        'methods'  => [      'set_lc_sc'],
        'values'   => [     'SubService'],
    ],


    'set_attribute_id' => [
        'template' =>
<<<END
                    {% set {subprefix}={subprefix}s[{prefix}.{sub_service}_id] %}

END,
        'patterns' => ['/{subprefix}/','/{sub_service}/'],
        'methods'  => [          'set',      'set_lc_sc'],
        'values'   => [    'subprefix',     'SubService'],
    ],

    'attribute' => [
        'template' =>
<<<END
                        <td class="text-center">{{ {prefix}.{attribute} }}</td>

END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

    'attribute_id' => [
        'template' =>
<<<END
                        <td class="text-center">{{ attribute({subprefix}, 'id') }}</td>

END,
        'patterns' => ['/{subprefix}/'],
        'methods'  => [          'set'],
        'values'   => [    'subprefix'],
    ],
];
