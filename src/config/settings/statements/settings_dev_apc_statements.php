<?php

return [

    'api_endpoints' => [
        'template' =>
            <<<END
    '{subprefix}_api_endpoints' => [
        'base_url' => \$_ENV['BASE_API_URL'],
        'get_all_uri' => '/api-{subprefix}s',
    ], \n
END,
        'patterns' => ['/{subprefix}/'],
        'methods'  => [          'set'],
        'values'   => [    'subPrefix'],
    ],
];
