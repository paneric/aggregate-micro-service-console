<?php

return [
    'ams' => [
        'files' => [
            APP_FOLDER . 'resources_ams/Gateway/AggregateServiceDAO.php'  => 'dao',
            APP_FOLDER . 'resources_ams/Gateway/AggregateServiceADAO.php' => 'adao',

            APP_FOLDER . 'resources_ams/AggregateServiceApc/config/dependencies/action.php' => 'dependency_action_apc',

            APP_FOLDER . 'resources_ams/config/module-resolver-config.php' => 'module_resolver_config',

            APP_FOLDER . 'resources_ams/AggregateServiceApc/config/settings/dev.php' => 'settings_dev_apc',
        ],

        'params' => [
            'patterns' => array_merge(
                ['/{Paneric}/', '/{vendorServicePath}/', '/{aggregate_service}/', '/{AggregateService}/', '/{prefix}/', '/{table_name}/'],
                ['/{left_relation}/', '/{right_relation}/', '/{left_prefix}/', '/{right_prefix}/']
            ),
            'methods'  => array_merge(
                ['set', 'set', 'set_lc_sc', 'set', 'set', 'set'],
                ['set_lc_sc', 'set_lc_sc', 'set', 'set']
            ),
            'values'   => array_merge(
                ['vendor', 'vendorServicePath', 'service', 'service', 'prefix', 'tableName'],
                ['leftRelation', 'rightRelation', 'leftPrefix', 'rightPrefix']
            ),
        ],

        'dao' => require 'statements/dao_statements.php',
        'adao' => require 'statements/adao_statements.php',

        'dependency_action_apc' => require 'statements/dependency_action_apc_statements.php',

        'module_resolver_config' => require 'statements/module_resolver_config_statements.php',

        'settings_dev_apc'    => require 'statements/settings_dev_apc_statements.php',
    ],
];
