<?php

declare(strict_types=1);

use Paneric\AMSConsole\Service\FilesScanner;
use Paneric\AMSConsole\Service\AMSService;
use Paneric\AMSConsole\Service\PatternsConverter;
use Paneric\AMSConsole\Service\Statement\Preparator\ADAOPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\DAOPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\DependencyPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\ModuleResolverConfigPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\SettingsDevPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\TemplatesShowAllPaginatedPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\TemplatesShowAllPreparator;
use Paneric\AMSConsole\Service\Statement\StatementsConverter;
use Paneric\AMSConsole\Service\Statement\StatementsPreparator;
use Psr\Container\ContainerInterface;

return [
    AMSService::class => static function (ContainerInterface $container): AMSService
    {
        return new AMSService(
            $container->get(StatementsPreparator::class),
            $container->get(PatternsConverter::class),
            $container->get(StatementsConverter::class),
            $container->get(FilesScanner::class),
            $container->get('ams'),
            $container->get('ams_resources_dir'),
            $container->get('ams_output_dir')
        );
    },

    FilesScanner::class => static function (): FilesScanner
    {
        return new FilesScanner();
    },

    PatternsConverter::class => static function (ContainerInterface $container): PatternsConverter
    {
        return new PatternsConverter(
            $container->get('ams')['params'],
        );
    },

    StatementsPreparator::class => static function (ContainerInterface $container): StatementsPreparator
    {
        return new StatementsPreparator(
            $container->get(DAOPreparator::class),
            $container->get(ADAOPreparator::class),
            $container->get(DependencyPreparator::class),
            $container->get(SettingsDevPreparator::class),
            $container->get(TemplatesShowAllPaginatedPreparator::class),
            $container->get(TemplatesShowAllPreparator::class),
            $container->get(ModuleResolverConfigPreparator::class),
            $container->get('ams')
        );
    },

    DAOPreparator::class => static function (): DAOPreparator
    {
        return new DAOPreparator();
    },
    ADAOPreparator::class => static function (): ADAOPreparator
    {
        return new ADAOPreparator();
    },
    DependencyPreparator::class => static function (): DependencyPreparator
    {
        return new DependencyPreparator();
    },
    SettingsDevPreparator::class => static function (): SettingsDevPreparator
    {
        return new SettingsDevPreparator();
    },
    TemplatesShowAllPaginatedPreparator::class => static function (): TemplatesShowAllPaginatedPreparator
    {
        return new TemplatesShowAllPaginatedPreparator();
    },
    TemplatesShowAllPreparator::class => static function (): TemplatesShowAllPreparator
    {
        return new TemplatesShowAllPreparator();
    },
    ModuleResolverConfigPreparator::class => static function (): ModuleResolverConfigPreparator
    {
        return new ModuleResolverConfigPreparator();
    },


    StatementsConverter::class => static function (): StatementsConverter
    {
        return new StatementsConverter();
    },
];
