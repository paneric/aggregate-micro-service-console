<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Command;

use Paneric\AMSConsole\Service\AMSService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Psr\Container\ContainerInterface;

class AMSCreateCommand extends Command
{
    protected static $defaultName = 'ams';

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Creates interfaces.')
            ->setHelp('This command allows you to create interfaces.')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Choice of required class.',
                'ams'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (SCOPE !== 'dev') {

            $questionHelper = $this->getHelper('question');
            $vendor = $questionHelper->ask(
                $input,
                $output,
                new Question('Psr-4 (no trailing slash !!!): ')
            );


            $vendorServicePath = $questionHelper->ask(
                $input,
                $output,
                new Question('Service vendor folder path (relative to project folder, no trailing slash !!!): ')
            );
            $service = $questionHelper->ask(
                $input,
                $output,
                new Question('Service name (CamelCase !!!): ')
            );
            $prefix = $questionHelper->ask(
                $input,
                $output,
                new Question('Service prefix (lower case !!!): ')
            );


            $vendorRelatedServicesPaths = $questionHelper->ask(
                $input,
                $output,
                new Question('Related services vendor folders paths (coma separated, relative to project folder, no trailing slash !!!): ')
            );
            $relatedServices = $questionHelper->ask(
                $input,
                $output,
                new Question('Related services names (CamelCase, coma separated !!!): ')
            );
            $relatedPrefixes = $questionHelper->ask(
                $input,
                $output,
                new Question('Related services prefixes (lower case, coma separated !!!): ')
            );


            $otherServices = $questionHelper->ask(
                $input,
                $output,
                new Question('Other services names (CamelCase, coma separated !!!): ')
            );


            $tableName = $questionHelper->ask(
                $input,
                $output,
                new Question('DB table name: ')
            );
        }


        $adapter = $this->container->get(AMSService::class);

        if ($adapter === null) {
            $this->setModeErrorOutput($output);

            return 0;
        }

        if (SCOPE === 'dev') {
            $vendor ='ECommerce';
            $vendorServicePath ='vendor/paneric/e-commerce-user-address';
            $service ='UserAddress';
            $prefix ='ua';
            $vendorRelatedServicesPaths ='vendor/paneric/e-commerce-user,vendor/paneric/e-commerce-address';
            $relatedServices ='User,Address';
            $relatedPrefixes ='usr,adr';
            $otherServices ='ListCountry,ListTypeCompany,Credential';
            $tableName ='user_addresss';
        }



        $adapter->convert(
            $output,
            $vendor,
            $vendorServicePath,
            $service,
            $prefix,
            explode(',', str_replace(' ','', $vendorRelatedServicesPaths)),
            explode(',', str_replace(' ','', $relatedServices)),
            explode(',', str_replace(' ','', $relatedPrefixes)),
            explode(',', str_replace(' ','', $otherServices)),
            $tableName
        );

        return 0;
    }

    protected function setModeErrorOutput(OutputInterface $output): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('white', 'red', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  ERROR:                                                                  </>',
            '<title>                                                                          </>',
            '',
            $this->setModeErrorMessage(),
            ''
        ]);
    }

    protected function setModeErrorMessage(): string
    {
        return '<fg=red;options=bold> Something\'s wrong, check logs. </>';
    }
}

