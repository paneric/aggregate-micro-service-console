<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\Gateway;

use Paneric\DataObject\DAO;

class {AggregateService}DAO extends DAO
{
{attributes_id}

    public function __construct()
    {
        $this->prefix = '{prefix}_';

        $this->setMaps();
    }

{getters_id}

{setters_id}
}
