<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\Gateway;

{use}
use Paneric\DataObject\ADAO;

class {AggregateService}ADAO extends ADAO
{
{attributes}

    public function __construct()
    {
{construct}

        unset($this->values);
    }

{getters}

{setters}

    public function transform(bool $isSc = true): array
    {
        return [
{transform}
        ];
    }
}
