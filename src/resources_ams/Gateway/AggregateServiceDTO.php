<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\Gateway;

class {AggregateService}DTO extends {AggregateService}DAO {}
