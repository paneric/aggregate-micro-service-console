<?php

declare(strict_types=1);

return [
    'root_folder' => ROOT_FOLDER,

    'base_uri' => '',

    'main_route_name' => 'cms.main.index',

    'dbal' => [
        'limit' => 10,
        'host' => $_ENV['DB_HOST'],
        'charset' => 'utf8',
        'dbName' => $_ENV['DB_NAME'],
        'user' => $_ENV['DB_USR'],
        'password' => $_ENV['DB_PSWD'],
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::MYSQL_ATTR_FOUND_ROWS => true//count rowCounts even if identical values updated
        ],
    ],

    'twig-builder-app' => [
        'templates_dirs' => [
            'error' => APP_FOLDER . 'templates/error/',
            'module' => MODULE_TEMPLATES_FOLDER,
            'app' => APP_FOLDER . 'templates/',
        ],
        'options' => [
            'debug' => true, /* "prod" false */
            'charset' => 'UTF-8',
            'strict_variables' => false,
            'autoescape' => 'html',
            'cache' => false, /* "prod" ROOT_FOLDER.'/var/cache/twig'*/
            'auto_reload' => null,
            'optimizations' => -1,
        ],
    ],

    'session-wrapper' => [
        'table' => 'sessions',

        'cookie_lifetime' => '0',         // Expire on close (string value!!!)
        'cookie_access' => '/',         // SessionWrapper cookie readable in all folders.
        'domain' => '',                 // '' for localhost, '.exemple.com' for others
        'secure' => false,              // in case of https true
        'js_denied' => true,            // Make sure the session cookie is not accessible via javascript.

        'hash_function' => 'sha512',    // Hash algorithm to use for the session. (use hash_algos() to get a list of available hashes.)
        'hash_bits_per_character' => 6, // How many bits per character of the hash. The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
        'use_only_cookies' => 1,        // Force the session to only use cookies, not URL variables.
        'gc_maxlifetime' => '86400',      // session max lifetime

        'cookie_name' => 'scm',         // session cookie name

        'encrypt' => true,
        'regenerate' => false,          // Adviced true, but somehow ajax searcher does not work with it.
    ],

    'guard' => [
        'key_ascii' => $_ENV['KEY_ASCII'],
        'algo_password' => PASSWORD_BCRYPT,
        'options_algo_password' => [
            'cost' => 10,
        ],
        'algo_hash' => 'sha512',
        'unique_id_prefix' => '',
        'unique_id_more_entropy' => true,
    ],

    'csrf' => [
        'csrf_key_name' => 'csrf_key',
        'csrf_key_length' => 32,
        'csrf_hash_name' => 'csrf_hash',
    ],

    'pagination-extension' => [
        'links_number' => 2,
        'nav_tag_open' => '<nav class="nav-pagination"><ul class="pagination">',
        'first_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'first_tag_middle' => '">',
        'first_tag_close' => '</a></li>',
        'tag_open' => '<li class="page-item"><a class="page-link" href="',
        'tag_middle' => '">',
        'tag_close' => '</a></li>',
        'current_tag_open' => '<li class="page-item"><a class="page-link-current" href="',
        'current_tag_middle' => '">',
        'current_tag_close' => '<span class="sr-only">(current)</span></a></li>',
        'last_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'last_tag_middle' => '">',
        'last_tag_close' => '</a></li>',
        'nav_tag_close' => '</ul></nav>',
    ],

    'guzzle_client' => [],

    'api_endpoints' => [
        'base_url' => $_ENV['BASE_API_URL'],
        'get_all_paginated_uri' => '/api-{prefix}s/',
        'origin_get_all_paginated_uri' => '/apc-{prefix}s/show-all-paginated/',
        'get_all_by_paginated_uri' => '/api-{prefix}s/',
        'origin_get_all_by_paginated_uri' => '/apc-{prefix}s/show-all-by-paginated/',
        'get_one_by_ids_uri' => '/api-{prefix}/',
    ],

{api_endpoints}
    'jwt_authentication' => [
        'secret' => $_ENV['JWT_SECRET'],
        'algorithm' => 'HS256',
        'secure' => false, // only for localhost for prod and test env set true
        'error' => static function ($response, $arguments) {
            $data['status'] = 401;
            $data['error'] = 'Unauthorized/'. $arguments['message'];
            return $response
                ->withHeader('Content-Type', 'application/json;charset=utf-8')
                ->getBody()->write(json_encode(
                    $data,
                    JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                ));
        }
    ],
];
