<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Middleware\AuthenticationMiddleware;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Middleware\UriMiddleware;
use Paneric\Session\SessionMiddleware;

return [
    RouteMiddleware::class => static function (Container $container): RouteMiddleware {
        return new RouteMiddleware($container);
    },

    UriMiddleware::class => static function (Container $container): UriMiddleware {
        return new UriMiddleware($container);
    },

    SessionMiddleware::class => static function (Container $container): SessionMiddleware {
        return new SessionMiddleware($container);
    },

    AuthenticationMiddleware::class => static function (Container $container): AuthenticationMiddleware {
        return new AuthenticationMiddleware($container->get(SessionInterface::class));
    },

    CSRFMiddleware::class => static function (Container $container): CSRFMiddleware {
        $config = $container->get('csrf');

        return new CSRFMiddleware(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $config
        );
    },

    JWTAuthenticationEncoderMiddleware::class => static function (Container $container): JWTAuthenticationEncoderMiddleware {
        $config = $container->get('jwt_authentication');

        return new JWTAuthenticationEncoderMiddleware(
            $container->get(SessionInterface::class),
            $config['secret'],
            $config['algorithm']
        );
    },
];
