<?php

declare(strict_types=1);

use {Paneric}\{AggregateService}\{AggregateService}Apc\Action\{AggregateService}GetAllByPaginatedApcAction;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use {Paneric}\{AggregateService}\{AggregateService}Apc\Action\{AggregateService}GetAllPaginatedApcAction;
use {Paneric}\{AggregateService}\{AggregateService}Apc\Action\{AggregateService}GetOneByIdsApcAction;

return [
    '{aggregate_service}_show_all_paginated_action' => static function (Container $container): {AggregateService}GetAllPaginatedApcAction {
        return new {AggregateService}GetAllPaginatedApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => '{aggregate_service}',
                    'prefix' => '{prefix}',
                    'prefixes' => ['{prefix}'{related_prefixes}] // main prefix first !!!
                ]
            )

        );
    },
    '{aggregate_service}_show_all_by_paginated_action' => static function (Container $container): {AggregateService}GetAllByPaginatedApcAction {
        return new {AggregateService}GetAllByPaginatedApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => '{aggregate_service}',
                    'prefix' => '{prefix}',
                    'prefixes' => ['{prefix}'{related_prefixes}] // main prefix first !!!
                ]
            )
        );
    },
    '{aggregate_service}_show_one_by_ids_action' => static function (Container $container): {AggregateService}GetOneByIdsApcAction {
        return new {AggregateService}GetOneByIdsApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => '{aggregate_service}',
                    'prefix' => '{prefix}',
                ]
            )
        );
    },
];
