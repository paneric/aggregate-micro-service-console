<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use {Paneric}\{AggregateService}\{AggregateService}Apc\{AggregateService}ApcController;
use Twig\Environment as Twig;

return [
    '{aggregate_service}_controller' => static function (Container $container): {AggregateService}ApcController {
        return new {AggregateService}ApcController(
            $container->get(Twig::class),
            'apc-{prefix}'
        );
    },
];
