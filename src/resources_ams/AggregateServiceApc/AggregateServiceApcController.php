<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Apc;

use Paneric\AggregateModule\Module\Controller\ModuleApcController;

class {AggregateService}ApcController extends ModuleApcController
{}
