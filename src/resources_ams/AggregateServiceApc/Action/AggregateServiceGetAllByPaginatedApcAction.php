<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Apc\Action;

use Paneric\AggregateModule\Module\Action\Apc\GetAllByPaginatedApcAction;
use {Paneric}\{AggregateService}\{AggregateService}Apc\Interfaces\Action\{AggregateService}GetAllByPaginatedApcActionInterface;

class {AggregateService}GetAllByPaginatedApcAction
    extends GetAllByPaginatedApcAction
    implements {AggregateService}GetAllByPaginatedApcActionInterface
{}
