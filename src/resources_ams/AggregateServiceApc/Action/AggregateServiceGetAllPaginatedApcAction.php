<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Apc\Action;

use Paneric\AggregateModule\Module\Action\Apc\GetAllPaginatedApcAction;
use {Paneric}\{AggregateService}\{AggregateService}Apc\Interfaces\Action\{AggregateService}GetAllPaginatedApcActionInterface;

class {AggregateService}GetAllPaginatedApcAction
    extends GetAllPaginatedApcAction
    implements {AggregateService}GetAllPaginatedApcActionInterface
{}
