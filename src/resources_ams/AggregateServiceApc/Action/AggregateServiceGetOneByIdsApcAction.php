<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Apc\Action;

use Paneric\AggregateModule\Module\Action\Apc\GetOneByIdsApcAction;
use {Paneric}\{AggregateService}\{AggregateService}Apc\Interfaces\Action\{AggregateService}GetOneByIdsApcActionInterface;

class {AggregateService}GetOneByIdsApcAction
    extends GetOneByIdsApcAction
    implements {AggregateService}GetOneByIdsApcActionInterface
{}
