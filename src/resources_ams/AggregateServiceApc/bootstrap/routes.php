<?php

declare(strict_types=1);

use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->get('/apc-{prefix}/show-all-by-paginated/{relation}/{id}[/{page}]', function (Request $request, Response $response, array $args) {
        return $this->get('{aggregate_service}_controller')->showAllByPaginated(
            $request,
            $response,
            $this->get('{aggregate_service}_show_all_by_paginated_action'),
            $args['relation'],
            $args['id'],
            empty($args) ? null : $args['page']
        );
    })->setName('apc-{prefix}.show_all_by_paginated')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->get('/apc-{prefix}/show-one-by-ids/{left_id}/{right_id}', function (Request $request, Response $response, array $args) {
        return $this->get('{aggregate_service}_controller')->showOneByIds(
            $request,
            $response,
            $this->get('{aggregate_service}_show_one_by_ids_action'),
            $args['left_id'],
            $args['right_id']
        );
    })->setName('apc-{prefix}.show_one_by_ids')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}
