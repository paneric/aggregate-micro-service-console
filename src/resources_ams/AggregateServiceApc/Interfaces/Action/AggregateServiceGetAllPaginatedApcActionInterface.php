<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Apc\Interfaces\Action;

use Paneric\AggregateModule\Interfaces\Action\Apc\GetAllPaginatedApcActionInterface;

interface {AggregateService}GetAllPaginatedApcActionInterface extends GetAllPaginatedApcActionInterface
{}
