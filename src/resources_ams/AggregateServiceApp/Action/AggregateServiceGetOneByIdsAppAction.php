<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}App\Action;

use Paneric\AggregateModule\Module\Action\App\GetOneByIdsAppAction;
use {Paneric}\{AggregateService}\{AggregateService}App\Interfaces\Action\{AggregateService}GetOneByIdsAppActionInterface;

class {AggregateService}GetOneByIdsAppAction
    extends GetOneByIdsAppAction
    implements {AggregateService}GetOneByIdsAppActionInterface
{}
