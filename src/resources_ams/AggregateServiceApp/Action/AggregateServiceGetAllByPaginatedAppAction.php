<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}App\Action;

use {Paneric}\{AggregateService}\{AggregateService}App\Interfaces\Action\{AggregateService}GetAllByPaginatedAppActionInterface;
use Paneric\AggregateModule\Module\Action\App\GetAllByPaginatedAppAction;

class {AggregateService}GetAllByPaginatedAppAction
    extends GetAllByPaginatedAppAction
    implements {AggregateService}GetAllByPaginatedAppActionInterface
{}
