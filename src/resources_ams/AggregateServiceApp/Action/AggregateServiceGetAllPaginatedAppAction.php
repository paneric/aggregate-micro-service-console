<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}App\Action;

use Paneric\AggregateModule\Module\Action\App\GetAllPaginatedAppAction;
use {Paneric}\{AggregateService}\{AggregateService}App\Interfaces\Action\{AggregateService}GetAllPaginatedAppActionInterface;

class {AggregateService}GetAllPaginatedAppAction
    extends GetAllPaginatedAppAction
    implements {AggregateService}GetAllPaginatedAppActionInterface
{}
