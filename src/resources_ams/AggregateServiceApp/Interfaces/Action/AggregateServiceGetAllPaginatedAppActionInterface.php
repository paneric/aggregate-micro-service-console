<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}App\Interfaces\Action;

use Paneric\AggregateModule\Interfaces\Action\App\GetAllPaginatedAppActionInterface;

interface {AggregateService}GetAllPaginatedAppActionInterface extends GetAllPaginatedAppActionInterface
{}
