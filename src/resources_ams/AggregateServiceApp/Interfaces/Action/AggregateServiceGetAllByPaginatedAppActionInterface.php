<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}App\Interfaces\Action;

use Paneric\AggregateModule\Interfaces\Action\App\GetAllByPaginatedAppActionInterface;

interface {AggregateService}GetAllByPaginatedAppActionInterface extends GetAllByPaginatedAppActionInterface
{}
