<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}App\Interfaces\Action;

use Paneric\AggregateModule\Interfaces\Action\App\GetOneByIdsAppActionInterface;

interface {AggregateService}GetOneByIdsAppActionInterface extends GetOneByIdsAppActionInterface
{}
