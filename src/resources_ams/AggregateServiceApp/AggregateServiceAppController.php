<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}App;

use Paneric\AggregateModule\Module\Controller\ModuleAppController;

class {AggregateService}AppController extends ModuleAppController
{}
