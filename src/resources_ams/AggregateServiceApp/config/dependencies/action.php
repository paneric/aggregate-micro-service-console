<?php

declare(strict_types=1);

use {Paneric}\{AggregateService}\{AggregateService}App\Action\{AggregateService}GetAllByPaginatedAppAction;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Session\SessionInterface;
use {Paneric}\{AggregateService}\{AggregateService}App\Action\{AggregateService}GetAllPaginatedAppAction;
use {Paneric}\{AggregateService}\{AggregateService}App\Action\{AggregateService}GetOneByIdsAppAction;

return [
    '{aggregate_service}_get_all_paginated_action' => static function (Container $container): {AggregateService}GetAllPaginatedAppAction {
        return new {AggregateService}GetAllPaginatedAppAction(
            $container->get('{aggregate_service}_query'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => '{aggregate_service}',
                'prefix' => '{prefix}',
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (): array
                {
                    return [];
                },
            ]
        );
    },
    '{aggregate_service}_get_all_by_paginated_action' => static function (Container $container): {AggregateService}GetAllByPaginatedAppAction {
        return new {AggregateService}GetAllByPaginatedAppAction(
            $container->get('{aggregate_service}_query'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => '{aggregate_service}',
                'prefix' => '{prefix}',
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (): array
                {
                    return [];
                },
            ]
        );
    },
    '{aggregate_service}_get_one_by_ids_action' => static function (Container $container): {AggregateService}GetOneByIdsAppAction {
        return new {AggregateService}GetOneByIdsAppAction(
            $container->get('{aggregate_service}_query'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => '{aggregate_service}',
                'prefix' => '{prefix}',
                'find_one_by_criteria' => static function (String $leftId, String $rightId): array
                {
                    return [
                        '{prefix}_{left_relation}_id' => $leftId,
                        '{prefix}_{right_relation}_id' => $rightId,
                    ];
                },
            ]
        );
    },
];
