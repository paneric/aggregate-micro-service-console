<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use {Paneric}\{AggregateService}\{AggregateService}App\{AggregateService}AppController;
use Twig\Environment as Twig;

return [
    '{aggregate_service}_controller' => static function (Container $container): {AggregateService}AppController {
        return new {AggregateService}AppController(
            $container->get(Twig::class),
            '{prefix}'
        );
    },
];
