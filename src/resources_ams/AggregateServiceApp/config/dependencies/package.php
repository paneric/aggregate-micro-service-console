<?php

declare(strict_types=1);

use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\Guard\Guard;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Pagination\PaginationExtension;
use Paneric\Session\DBAL\DBALAdapter;
use Paneric\Session\DBAL\SessionRepository;
use Paneric\Session\SessionWrapper;
use Paneric\Twig\Extension\SessionExtension;
use Paneric\Twig\TwigBuilder;
use Slim\App;
use Slim\Factory\AppFactory;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Translator;
use Twig\Environment as Twig;
use Twig\Extension\DebugExtension;
use Paneric\Twig\Extension\CSRFExtension;

return [
    App::class => static function (Container $container) {
        AppFactory::setContainer($container);

        return AppFactory::create();
    },

    'route_parser_interface' => static function (Container $container) {
        $app = $container->get(App::class);

        return $app->getRouteCollector()->getRouteParser();
    },

    Manager::class => static function (Container $container): Manager
    {
        $pdoBuilder = new PDOBuilder();

        return new Manager(
            $pdoBuilder->build($container->get('dbal')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },

    GuardInterface::class => static function (Container $container): Guard
    {
        $randomFactory = new RandomLib\Factory;

        return new Guard(
            $randomFactory->getMediumStrengthGenerator(),
            $container->get('guard')
        );
    },

    'Twig_init' => static function (Container $container): Twig
    {
        $twigBuilder = new TwigBuilder();

        $twigBuilderSettings = [];

        if ($container->has('twig-builder-module')) {
            $twigBuilderSettings = $container->get('twig-builder-module');
        }

        if ($twigBuilderSettings === [] && $container->has('twig-builder-app')) {
            $twigBuilderSettings = $container->get('twig-builder-app');
        }

        $app = $container->get(App::class);
        $basePath = $app->getBasePath();

        $environment = $twigBuilder->build($twigBuilderSettings, $basePath);

        $sessionExtension = new SessionExtension(
            $container->get(SessionInterface::class),
            $container->get(Translator::class)
        );
        $environment->addExtension($sessionExtension);

        $translationExtension = new TranslationExtension($container->get(Translator::class));
        $environment->addExtension($translationExtension);

        $debugExtension = new DebugExtension();
        $environment->addExtension($debugExtension);

        return $environment;
    },

    SessionInterface::class => static function (Container $container): ?SessionWrapper
    {
        $pdoAdapter = new DBALAdapter(
            $container->get(Manager::class),
            new SessionRepository($container->get(Manager::class)),
            $container->get(GuardInterface::class)
        );

        return new SessionWrapper(
            $container->get('session-wrapper'),
            $pdoAdapter
        );
    },

    Translator::class => static function (Container $container): Translator {
        $translator = new Translator((string) $container->get('local'));
        $translator->addLoader('array', new ArrayLoader());

        $translator->addResource(
            'array',
            $container->get('translation_module'),
            (string) $container->get('local')
        );

        return $translator;
    },

    Twig::class => static function (Container $container): Twig
    {
        $twig = $container->get('Twig_init');

        $twig->addExtension($container->get(CSRFExtension::class));
        $twig->addExtension($container->get(PaginationExtension::class));

        return $twig;
    },
];
