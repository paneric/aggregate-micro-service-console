<?php

declare(strict_types=1);

use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {
    $app->get('/{prefix}/show-all-by-paginated/{relation}/{id}[/{page}]', function (Request $request, Response $response, array $args) {
        return $this->get('{aggregate_service}_controller')->showAllByPaginated(
            $request,
            $response,
            $this->get('{aggregate_service}_get_all_by_paginated_action'),
            $args['relation'],
            $args['id']
        );
    })->setName('{prefix}.show_all_by_paginated')
        ->addMiddleware($container->get(PaginationMiddleware::class));

    $app->get('/{prefix}/show-one-by-ids/{left_id}/{right_id}', function (Request $request, Response $response, array $args) {
        return $this->get('{aggregate_service}_controller')->showOneByIds(
            $response,
            $this->get('{aggregate_service}_get_one_by_ids_action'),
            $args['left_id'],
            $args['right_id']
        );
    })->setName('{prefix}.show_one_by_ids');
}
