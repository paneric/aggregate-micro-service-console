<?php

declare(strict_types=1);

use Paneric\ModuleResolver\ModuleMapper;

$moduleMapper = new ModuleMapper();

$moduleMapCrossPaths = [
{vendor_related_services_paths}
];

$moduleMapPath = '{vendorServicePath}';

$moduleMap = [
    '{prefix}'      => ROOT_FOLDER . '%s/src/{AggregateService}App',
    'api-{prefix}'  => ROOT_FOLDER . '%s/src/{AggregateService}Api',
    'api-{prefix}s' => ROOT_FOLDER . '%s/src/{AggregateService}Api',
    'apc-{prefix}'  => ROOT_FOLDER . '%s/src/{AggregateService}Apc',
];

return [
    'default_route_key' => '{prefix}',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => true,

    'module_map' => $moduleMapper->setModuleMap($moduleMap),

    'module_map_cross' => $moduleMapper->setModuleMapCross(
        $moduleMapCrossPaths,
        $moduleMap,
        $moduleMapPath,
        __DIR__ !== ROOT_FOLDER . 'src/config'
    ),
];
