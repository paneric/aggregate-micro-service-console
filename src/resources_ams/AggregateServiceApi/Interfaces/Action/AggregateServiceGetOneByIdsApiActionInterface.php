<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Api\Interfaces\Action;

use Paneric\AggregateModule\Interfaces\Action\Api\GetOneByIdsApiActionInterface;

interface {AggregateService}GetOneByIdsApiActionInterface extends GetOneByIdsApiActionInterface
{}
