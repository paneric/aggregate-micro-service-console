<?php

declare(strict_types=1);

use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {

    $app->get('/api-{prefix}s/{page}', function (Request $request, Response $response, array $args) {
        return $this->get('{aggregate_service}_controller')->getAllPaginated(
            $request,
            $response,
            $this->get('{aggregate_service}_get_all_paginated_action'),
            $args['page']
        );
    })->setName('get.api-{prefix}s.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-{prefix}s/{relation}/{id}[/{page}]', function (Request $request, Response $response, array $args) {
        return $this->get('{aggregate_service}_controller')->getAllByPaginated(
            $request,
            $response,
            $this->get('{aggregate_service}_get_all_by_paginated_action'),
            $args['relation'],
            $args['id'],
            $args['page']
        );
    })->setName('get.api-{prefix}s.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-{prefix}/{left_id}/{right_id}', function (Request $request, Response $response, array $args) {
        return $this->get('{aggregate_service}_controller')->getOneByIds(
            $response,
            $this->get('{aggregate_service}_get_one_by_ids_action'),
            $args['left_id'],
            $args['right_id'],
        );
    })->setName('get.api-{prefix}')
        ->addMiddleware($container->get(JwtAuthentication::class));
}
