<?php

declare(strict_types=1);

use {Paneric}\{AggregateService}\Gateway\{AggregateService}ADAO;
use {Paneric}\{AggregateService}\Gateway\{AggregateService}DAO;
use {Paneric}\{AggregateService}\Repository\{AggregateService}Repository;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\DBAL\Manager;
use {Paneric}\{AggregateService}\Repository\{AggregateService}Query;

return [
    '{aggregate_service}_repository' => static function (Container $container): {AggregateService}Repository
    {
        return new {AggregateService}Repository(
            $container->get(Manager::class),
            [
                'table' => '{table_name}',//{aggregate_table}
                'dao_class' => {AggregateService}DAO::class,
                'create_unique_where' => '',
                'update_unique_where' => '',
            ]
        );
    },

    '{aggregate_service}_query' => static function (Container $container): {AggregateService}Query
    {
        return new {AggregateService}Query(
            $container->get(Manager::class),
            [
                'adao_class' => {AggregateService}ADAO::class,
                'base_query' => '',
            ]
        );
    },
];
