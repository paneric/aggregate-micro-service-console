<?php

declare(strict_types=1);

use {Paneric}\{AggregateService}\{AggregateService}Api\Action\{AggregateService}GetAllByPaginatedApiAction;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Session\SessionInterface;
use {Paneric}\{AggregateService}\{AggregateService}Api\Action\{AggregateService}GetAllPaginatedApiAction;
use {Paneric}\{AggregateService}\{AggregateService}Api\Action\{AggregateService}GetOneByIdsApiAction;

return [
    '{aggregate_service}_get_all_paginated_action' => static function (Container $container): {AggregateService}GetAllPaginatedApiAction {
        return new {AggregateService}GetAllPaginatedApiAction(
            $container->get('{aggregate_service}_query'),
            [
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (): array
                {
                    return [];
                },
            ]
        );
    },
    '{aggregate_service}_get_all_by_paginated_action' => static function (Container $container): {AggregateService}GetAllByPaginatedApiAction {
        return new {AggregateService}GetAllByPaginatedApiAction(
            $container->get('{aggregate_service}_query'),
            [
                'find_by_criteria' => static function (string $relation, string $id): array
                {
                    return [
                        '{prefix}_' . $relation . '_id' => (int) $id,
                    ];
                },
                'order_by' => static function (): array
                {
                    return [];
                },
            ]
        );
    },
    '{aggregate_service}_get_one_by_ids_action' => static function (Container $container): {AggregateService}GetOneByIdsApiAction {
        return new {AggregateService}GetOneByIdsApiAction(
            $container->get('{aggregate_service}_query'),
            [
                'find_one_by_criteria' => static function (string $leftId, string $rightId): array
                {
                    return [
                        '{prefix}_{left_relation}_id' => (int) $leftId,
                        '{prefix}_{right_relation}_id' => (int) $rightId,
                    ];
                },
            ]
        );
    },
];
