<?php

declare(strict_types=1);

use {Paneric}\{AggregateService}\{AggregateService}Api\{AggregateService}ApiController;

return [
    '{aggregate_service}_controller' => static function (): {AggregateService}ApiController {
        return new {AggregateService}ApiController('as');
    },
];
