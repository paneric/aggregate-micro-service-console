<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Api;

use Paneric\AggregateModule\Module\Controller\ModuleApiController;

class {AggregateService}ApiController extends ModuleApiController
{}
