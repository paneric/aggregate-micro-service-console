<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Api\Action;

use Paneric\AggregateModule\Module\Action\Api\GetOneByIdsApiAction;
use {Paneric}\{AggregateService}\{AggregateService}Api\Interfaces\Action\{AggregateService}GetOneByIdsApiActionInterface;

class {AggregateService}GetOneByIdsApiAction
    extends GetOneByIdsApiAction
    implements {AggregateService}GetOneByIdsApiActionInterface
{}
