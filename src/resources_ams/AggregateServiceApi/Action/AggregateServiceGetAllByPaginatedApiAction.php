<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Api\Action;

use Paneric\AggregateModule\Module\Action\Api\GetAllByPaginatedApiAction;
use {Paneric}\{AggregateService}\{AggregateService}Api\Interfaces\Action\{AggregateService}GetAllByPaginatedApiActionInterface;

class {AggregateService}GetAllByPaginatedApiAction
    extends GetAllByPaginatedApiAction
    implements {AggregateService}GetAllByPaginatedApiActionInterface
{}
