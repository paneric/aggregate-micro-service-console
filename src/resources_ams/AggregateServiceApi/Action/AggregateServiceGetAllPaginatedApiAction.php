<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\{AggregateService}Api\Action;

use Paneric\AggregateModule\Module\Action\Api\GetAllPaginatedApiAction;
use {Paneric}\{AggregateService}\{AggregateService}Api\Interfaces\Action\{AggregateService}GetAllPaginatedApiActionInterface;

class {AggregateService}GetAllPaginatedApiAction
    extends GetAllPaginatedApiAction
    implements {AggregateService}GetAllPaginatedApiActionInterface
{}
