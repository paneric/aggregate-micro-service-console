<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\Repository;

use Paneric\AggregateModule\Module\Repository\ModuleRepository;

class {AggregateService}Repository extends ModuleRepository implements {AggregateService}RepositoryInterface
{}
