<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\Repository;

use Paneric\AggregateModule\Interfaces\Repository\ModuleQueryInterface;

interface {AggregateService}QueryInterface extends ModuleQueryInterface
{}
