<?php

declare(strict_types=1);

namespace {Paneric}\{AggregateService}\Repository;

use Paneric\AggregateModule\Module\Repository\ModuleQuery;

class {AggregateService}Query extends ModuleQuery implements {AggregateService}QueryInterface
{}
