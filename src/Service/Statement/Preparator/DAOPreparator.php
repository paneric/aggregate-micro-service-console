<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service\Statement\Preparator;

class DAOPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $SubServices): array
    {
        $stringifiedStatements = [];

        foreach ($statements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWithSubServices($statement, $SubServices);
        }

        return $stringifiedStatements;
    }
}