<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service\Statement\Preparator;

class SettingsDevPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $subprefixes): array
    {
        $stringifiedStatements['api_endpoints'] = $this->prepareWithSubPrefixes(
            $statements['api_endpoints'],
            $subprefixes
        );

        return $stringifiedStatements;
    }
}