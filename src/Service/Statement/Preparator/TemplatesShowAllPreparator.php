<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service\Statement\Preparator;

class TemplatesShowAllPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $SubServices, array $attributes, array $subprefixes): array
    {
        $stringifiedStatements['title_attribute'] = $this->prepareWithAttributes(
            $statements['title_attribute'],
            $attributes
        );

        $stringifiedStatements['title_attribute_id'] = $this->prepareWithSubServices(
            $statements['title_attribute_id'],
            $SubServices
        );

        $stringifiedStatements['set_attribute_id'] = $this->prepareWithSubServicesSubPrefixes(
            $statements['set_attribute_id'],
            $SubServices,
            $subprefixes
        );

        $stringifiedStatements['attribute'] = $this->prepareWithAttributes(
            $statements['attribute'],
            $attributes
        );

        $stringifiedStatements['attribute_id'] = $this->prepareWithSubprefixes(
            $statements['attribute_id'],
            $subprefixes
        );


        return $stringifiedStatements;
    }
}
