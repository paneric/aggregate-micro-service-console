<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service\Statement\Preparator;

class ModuleResolverConfigPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $vendorRelatedServicesPaths): array
    {
        $stringifiedStatements['vendor_related_services_paths'] = $this->prepareWithArgument(
            $statements['vendor_related_services_paths'],
            $vendorRelatedServicesPaths,
            'vendorRelatedServicePath'
        );

        return $stringifiedStatements;
    }
}
