<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service\Statement\Preparator;

class DependencyPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $subprefixes): array
    {
        $stringifiedStatements = [];

        foreach ($statements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWithSubPrefixes(
                $statement,
                $subprefixes
            );
        }

        return $stringifiedStatements;
    }
}
