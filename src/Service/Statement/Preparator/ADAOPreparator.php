<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service\Statement\Preparator;

class ADAOPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, string $Vendor, array $SubServices): array
    {
        $stringifiedStatements = [];

        foreach ($statements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWithVendorSubServices($statement, $Vendor, $SubServices);
        }

        return $stringifiedStatements;
    }
}
