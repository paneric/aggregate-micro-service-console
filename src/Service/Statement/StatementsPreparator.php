<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service\Statement;

use Paneric\AMSConsole\Service\Statement\Preparator\ADAOPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\DAOPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\DependencyPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\ModuleResolverConfigPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\SettingsDevPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\TemplatesShowAllPaginatedPreparator;
use Paneric\AMSConsole\Service\Statement\Preparator\TemplatesShowAllPreparator;

class StatementsPreparator
{
    protected $daoPreparator;
    protected $adaoPreparator;
    protected $dependencyPreparator;
    protected $settingsDevPreparator;
    protected $templatesShowAllPaginatedPreparator;
    protected $templatesShowAllPreparator;
    protected $moduleResolverConfigPreparator;

    protected $settings;

    public function __construct(
        DAOPreparator $daoPreparator,
        ADAOPreparator $adaoPreparator,
        DependencyPreparator $dependencyPreparator,
        SettingsDevPreparator $settingsDevPreparator,
        TemplatesShowAllPaginatedPreparator $templatesShowAllPaginatedPreparator,
        TemplatesShowAllPreparator $templatesShowAllPreparator,
        ModuleResolverConfigPreparator $moduleResolverConfigPreparator,
        array $settings
    ) {
        $this->daoPreparator = $daoPreparator;
        $this->adaoPreparator = $adaoPreparator;
        $this->dependencyPreparator = $dependencyPreparator;
        $this->settingsDevPreparator = $settingsDevPreparator;
        $this->templatesShowAllPaginatedPreparator = $templatesShowAllPaginatedPreparator;
        $this->templatesShowAllPreparator = $templatesShowAllPreparator;
        $this->moduleResolverConfigPreparator = $moduleResolverConfigPreparator;

        $this->settings = $settings;
    }

    public function prepare(
        string $filePath,
        string $vendor,
        array $vendorRelatedServicesPaths,
        array $relatedServices,
        array $relatedPrefixes,
        array $otherServices
    ): array {
        $statements = [];

        if (isset($this->settings['files'][$filePath])) {
            $converter = $this->settings['files'][$filePath];

            if ($converter === 'dao') {
                $statements = $this->daoPreparator->prepare(
                    $this->settings['dao'],
                    $relatedServices
                );
            }

            if ($converter === 'adao') {
                $statements = $this->adaoPreparator->prepare(
                    $this->settings['adao'],
                    $vendor,
                    array_merge(
                        $relatedServices,
                        $otherServices
                    )
                );
            }


            if ($converter === 'dependency_action_apc') {
                $statements = $this->dependencyPreparator->prepare(
                    $this->settings['dependency_action_apc'],
                    $relatedPrefixes
                );
            }


            if ($converter === 'module_resolver_config') {
                $statements = $this->moduleResolverConfigPreparator->prepare(
                    $this->settings['module_resolver_config'],
                    $vendorRelatedServicesPaths
                );
            }


            if ($converter === 'settings_dev_apc') {
                $statements = $this->settingsDevPreparator->prepare(
                    $this->settings['settings_dev_apc'],
                    $relatedPrefixes
                );
            }
        }

        return $statements;
    }
}
