<?php

declare(strict_types=1);

namespace Paneric\AMSConsole\Service;

class PatternsConverter
{
    use ParamsTrait;
    use SettersTrait;

    protected $settings;

    public function __construct(array $settings
    ) {
        $this->settings = $settings;
    }

    public function convert(
        string $stringifiedFile,
        string $vendor,
        string $vendorServicePath,
        string $service,
        string $prefix,
        $relatedServices,
        $relatedPrefixes,
        string $tableName
    ): string {
        $extractor = [
            'methods' => $this->settings['methods'],
            'values' => $this->settings['values'],
            'vendor' => $vendor,
            'vendorServicePath' => $vendorServicePath,
            'service' => $service,
            'prefix' => $prefix,
            'leftRelation' => $relatedServices[0],
            'rightRelation' => $relatedServices[1],
            'leftPrefix' => $relatedPrefixes[0],
            'rightPrefix' => $relatedPrefixes[1],
            'tableName' => $tableName
        ];

        return preg_replace(
            $this->settings['patterns'],
            $this->setParams($extractor),
            $stringifiedFile
        );
    }
}
